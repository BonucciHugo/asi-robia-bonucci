# UDP

<br/>![](udp.png)

<br/>Cette partie permet d'afficher un message sur l'état du robot selon la décision du client.<br/>

<br/> Dans un terminal, **lancer** la commande `pyhton3 udp_server.py`
<br/>Puis dans un autre terminal, **lancer** la commande `pyhton3 udp_client.py`

### Explications

<br/>On demande au client de choisir la commande qu'il veut envoyer au robot : <br/>

```
action = input("To start robot press 1 or press 2 to stop robot:  ")

if action == '1':

    msgClient = "Start"

elif action == '2': 
    print("stop")
    msgClient = "Stop"

```
<br/>L'action est ensuite envoyer au serveur udp :<br/>
```
msgToSend = str.encode(msgClient)
addrPort = ("127.0.0.1", 9999)
bufferSize = 1024
# Créer un socket UDP coté client
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Envoyer au serveur à l'aide du socket UDP créé
s.sendto(msgToSend, addrPort)
```

<br/>Le serveur traite ensuite la demande pour envoyer le message correspondant à l'action demandée au client udp : <br/>
```
print(clientMsg)
    if clientMsg == "b'Start'":
        msg = str.encode("Sarting Robot")
    elif clientMsg == "b'Stop'":
        msg = str.encode("Stopping Robot")
    else :
        msg = str.encode("Hello Robot")
    # Envoi d'une réponse au client
    s.sendto(msg, address)
    
```
<br/>Le client affiche ensuite le message renvoyé par le serveur sur l'état du robot : <br/>  

```
    msgServer = s.recvfrom(bufferSize)
msg = "{}".format(msgServer[0])
print(msg)


