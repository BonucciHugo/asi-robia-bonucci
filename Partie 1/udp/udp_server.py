import socket


# Créer une socket datagramme
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# Lier à l'adresse IP et le port
s.bind(("127.0.0.1", 9999))

# Écoutez les datagrammes entrants
while(True):

    addr = s.recvfrom(1024)
    message = addr[0]
    address = addr[1]
    clientMsg = "{}".format(message)
    
    print(clientMsg)
    if clientMsg == "b'Start'":
        msg = str.encode("Sarting Robot")
    elif clientMsg == "b'Stop'":
        msg = str.encode("Stopping Robot")
    else :
        msg = str.encode("Hello Robot")
    # Envoi d'une réponse au client
    s.sendto(msg, address)