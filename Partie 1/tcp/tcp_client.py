import socket

HOST = '127.0.0.1'
PORT = 6789

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))
print ('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')

message = str.encode('Start')
n = client.send(message)
donnees = client.recv(1024)
print ('Recu :'), donnees
client.close()