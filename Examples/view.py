# api/cv/views.py
import os
from flask import send_from_directory
from werkzeug.utils import secure_filename

from api import (
    app,
    allowed_file,
    OUTPUTS_DIR
)
from api.cv.utils import (
    open_image, 
    image_from_buffer,
    faces_from_frame
)

@app.route('/api/opencv-stream', methods=['POST'])
def opencv_upload_stream():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return {"detail": "No file found"}, 400
        file = request.files['file']
        if file.filename == '':
            return {"detail": "Invalid file or filename missing"}, 400
        if file and not allowed_file(file.filename):
            return {"detail": "This file is not allowed"}, 401
        im = image_from_buffer(file)
        paths = faces_from_frame(im, save=True, destination=OUTPUTS_DIR)
        response_paths = []
        for path in paths:
            relative_path = path.replace(OUTPUTS_DIR, '')
            live_url = url_for('serve_result_dir', relative_path)
            response_paths.append(live_url)
        return {"paths": response_paths}, 201

@app.route('/api/opencv', methods=['POST'])
def opencv_upload():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return {"detail": "No file found"}, 400
        file = request.files['file']
        if file.filename == '':
            return {"detail": "Invalid file or filename missing"}, 400
        if file and not allowed_file(file.filename):
            return {"detail": "This file is not allowed"}, 401
        filename = secure_filename(file.filename)
        _, ext = os.path.splitext(filename)
        destination_folder = app.config['UPLOAD_FOLDER']
        new_filename = f"{len(os.listdir(destination_folder))}{ext}"
        save_path = os.path.join(destination_folder, new_filename)
        file.save(save_path)
        im = open_image(save_path)
        paths = faces_from_frame(im, save=True, destination=OUTPUTS_DIR)
        response_paths = []
        for path in paths:
            relative_path = path.replace(OUTPUTS_DIR, '')
            live_url = url_for('serve_opencv_result', relative_path)
            response_paths.append(live_url)
        return {"paths": response_paths}, 201
        '''
        # respond with one of the faces
        final_img = paths[0]
        relative_path = final_img.replace(OUTPUTS_DIR, "")
        if relative_path.startswith("/"):
            relative_path = relative_path[1:]
        return send_from_directory(OUTPUTS_DIR, relative_path)
        '''
