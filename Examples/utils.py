# api/cv/utils.py

import os
import cv2
import numpy as np

def list_options():
    for x in os.listdir(cv2.data.haarcascades):
        print(x)

def get_face_cascade(cascade='haarcascade_frontalface_default.xml'):
    return os.path.join(cv2.data.haarcascades, cascade)

def open_image(path):
    '''
    If we save the file locally, we can use this one.
    '''
    frame = cv2.imread(image_path)
    return frame

def image_from_buffer(file_buffer):
    '''
    If we don't save the file locally and just want to open
    a POST'd file. This is what we use.
    '''
    bytes_as_np_array = np.frombuffer(buffer.read(), dtype=np.uint8)
    flag = 1
    # flag = 1 == cv2.IMREAD_COLOR
    # https://docs.opencv.org/4.2.0/d4/da8/group__imgcodecs.html
    frame = cv2.imdecode(bytes_as_np_array, flag)
    return frame

def faces_from_frame(frame, save=True, destination=None):
    '''
    This is will extract all faces found in an image
    And save the faces (just the face) as a unique file
    in our destination folder.
    '''
    gray  = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cascade_xml = get_face_cascade()
    cascade = cv2.CascadeClassifier(cascade_xml)
    faces = cascade.detectMultiScale(frame, scaleFactor=1.5, minNeighbors=5)
    final_dest = None
    if destination is not None:
        secondary = len(os.listdir(destination)) + 1
        final_dest = os.path.join(destination, f"{secondary}")
        os.makedirs(final_dest, exist_ok=True)
    paths = []
    for i, (x, y, w, h) in enumerate(faces):
        roi_color = frame[y:y+h, x:x+w]
        if save == True and destination != None:
            current_face_path = os.path.join(root_path, f"{i}.jpg") 
            cv2.imwrite(current_face_path, roi_color)
            paths.append(this_path)
    return paths
