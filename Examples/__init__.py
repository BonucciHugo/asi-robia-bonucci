# api/__init__.py
import os
from flask import Flask, flash, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename

BASE_DIR = os.dirname(os.path.abspath(__file__))
UPLOAD_DIR = os.path.join(BASE_DIR, "storage", "uploads")
RESULTS_DIR = os.path.join(BASE_DIR, "storage", "results")
OUTPUTS_DIR = os.path.join(BASE_DIR, "storage", "outputs")
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

for d in [UPLOAD_DIR, RESULTS_DIR, OUTPUTS_DIR]:
    os.makedirs(d, exist_ok=True)

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

app.config['OUTPUTS_DIR'] = OUTPUTS_DIR

# Directly from the docs
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/static/uploads/<filename>')
def serve_upload_dir(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)
                            
@app.route('/static/results/<filename>')
def serve_result_dir(filename):
    return send_from_directory(app.config['OUTPUTS_DIR'],
                               filename)

from .views import *
from .cv.views import *
