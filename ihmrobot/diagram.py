# diagram.py
from flask import app
from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB

with Diagram("IHM Robot", show=False, direction="TB"):
    lb = ELB("IHM Robot")

    

    lb >> EC2("temperature") 

    EC2("camera") >> EC2("retour camera") >> lb


    lb >> EC2 ("message") >> EC2("state") >> lb

    EC2 ("Capteur temperature") >> EC2("temperature") >> lb
    