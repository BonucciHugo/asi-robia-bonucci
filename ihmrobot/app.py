#!/usr/bin/env python
from flask import Flask, render_template, Response, request
import cv2, random


app = Flask(__name__)

camera = cv2.VideoCapture(0)

def gen_frames():  
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result


@app.route('/')
def default():
  return render_template('index.html')


@app.route("/state")
def state():
  return render_template('state.html')

@app.route("/message", methods=['GET', 'POST'])
def message () :

  if request.method == 'POST':
    state = request.form['state']

  if state == "START":
    msg = "Bonjour vous venez de démarrer le robot"

  if state == "STOP" :
    msg = "Merci d'avoir arrêter le robot, anrevoir"

  return msg

@app.route("/ret_camera")
def ret_cam():

   return render_template('camera.html')

@app.route("/cam")
def cam():

   return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route("/temp")
def temp():

  return render_template('temp.html') #fichier a coder pour verif temperature robot

@app.route("/consigne")
def temperature():

  temp = random.randint(20,80)

  if temp >= 20 & temp < 40:
    alerte = "Attention le robot se réchauffe"
  if temp >= 40 & temp < 65: 
    alerte = "Il faut refroidir le robot immédiatement"
  if temp >= 65 : 
    alerte = "Le robot va cramer c'est foutu, tous à l'abri"
    
  return alerte 
  

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080) #lancement app -> localhost sur le port 8080