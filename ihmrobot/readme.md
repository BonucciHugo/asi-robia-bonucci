# IHM ROBOT

Cette IHM est une petite API flask permettant la gestion ou la visualisation des éléments d'un Robot à partir d'un navigateur web.<br/>
Pour utiliser cette IHM il faut avoir installer flask préalablement (pip install flask) et opencv (pip install opencv-python) pour la gestion de la caméra.<br/>

## Instruction

**Cloner** tout d'abord le git et placez vous dans le répertoire `asi-robia-bonucci/ihmrobot` depuis un terminal.<br/>

<br/>**Lancer** la commande **`pyhton3 app.py`** <br/>

<br/>**Ouvrer** un navigateur et taper **localhost:8080** dans la barre d'url ou bien **127.0.0.1:8080** <br/>

<br/> Vous devez avoir l'affichage **suivant** :<br/>

![](capture.png) 
<br/>
## Explications

Une fois sur cette interface de gestion, plusieurs possibilités s'offrent à vous.<br/>

### <br/>**`Robot state`**<br/>

<br/>![](state.png)

<br/>En **cliquant** sur `Robot state` vous lancer la route `/state` côté flask. Cette route permet simplement la redirection vers le bon template.<br/>

<br/>Une fois sur l'interface de ̀̀`Robot state`, vous pouvez choisir d'envoyer une action "**START**" ou "**STOP**" au robot (**attention** sensible à la casse)<br/>

<br/>Une fois l'action saisie, **Appuyer** sur `Entrer`<br/>

<br/>Cela, enverra en **POST** l'action saisie par l'utilisateur et sera traité dans le code ci-dessous :<br/>

```
@app.route("/message", methods=['GET', 'POST'])
def message () :

  if request.method == 'POST':
    state = request.form['state']

  if state == "START":
    msg = "Bonjour vous venez de démarrer le robot"

  if state == "STOP" :
    msg = "Merci d'avoir arrêter le robot, anrevoir"

  return msg
```
<br/>Le code renverra le message correspondant à l'action demandé par l'utilisateur<br/>

### <br/>**`Robot camera`**<br/>

<br/>![](camera.png)

<br/> La page robot camera affiche en temps réel le retour de la webcam branché au PC

Le traitement du flux video de la camera se fait grâce au code ci-dessous :
```
camera = cv2.VideoCapture(0)

def gen_frames():  
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
```
Ce traitement permet le bon affichage sur l'interface web ensuite<br/>

Pour l'affichage, on effectue le retour camera sur la route `/cam` <br/>

```
@app.route("/cam")
def cam():

   return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')
```
<br/>Le retour camera est ensuite affiché sur la page `camera.html`<br/>

```
<html>
  <head>
    <title>Camera</title>
    <link rel="stylesheet" type="text/css" href= "{{ url_for('static',filename='css/style.css') }}">
  </head>
  <body>
    <h1>Camera du robot</h1>
    <div>
      <img src="{{ url_for('cam') }}">
    </div>
    
    <p>
        <a href="/">Go back</a><br />
    </p>
  </body>
</html>
```
**Attention** pour que l'affichage fonctionne il faut que le nom 'name' dans {{url_for('name')}} soit le nom de la **fonction** et non celui de la route.<br/>

### <br/>**`Robot temperature`**<br/>

La partie robot temperature n'est pas fonctionnel mais l'objectif était de simuler l'état d'un capteur de température.<br/>
Puis afficher selon la température différent message d'alerte sur l'interface web.<br/>
Enfin, pouvoir réguler la température selon le message d'alerte afficher. 

```
@app.route("/temp")
def temp():

  return render_template('temp.html') #fichier a coder pour verif temperature robot

@app.route("/consigne")
def temperature():

  temp = random.randint(20,80)

  if temp >= 20 & temp < 40:
    alerte = "Attention le robot se réchauffe"
  if temp >= 40 & temp < 65: 
    alerte = "Il faut refroidir le robot immédiatement"
  if temp >= 65 : 
    alerte = "Le robot va cramer c'est foutu, tous à l'abri"
    
  return alerte 
```
Vous êtes désormais prêt à utiliser l'API flask pour la gestion du robot.<br/>

**NB** : Les fichiers HTML sont stockés dans le répertoire "templates" et tous les autres fichier HTML doivent également être stockés dans ce répertoire<br/>

<br/>Diagramme de fonctionnement de l'API :

<br/>![](api.png)
